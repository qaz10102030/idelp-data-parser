# -*- coding: utf-8 -*-
"""
Created on Wed Aug 12 16:54:01 2020

@author: 貓O
"""
import os
import pandas as pd
import numpy as np


def process_char_of_btl_set(keys):
    set_char = []
    for i in hunt[keys]:
        _char, _, _lv, _ = tuple((np.squeeze(btl_set[btl_set["Id"] == i].values)))
        # local = []
        set_str = ""
        for _c, _l in zip(_char, _lv):
            _, _job, _attr, _skill_set, _name = tuple(
                (np.squeeze(char[char["Id"] == _c].values))
            )
            # local.append({
            #     '名稱': _name,
            #     '職業': _job,
            #     '屬性': _attr,
            #     '等級': _l
            # })
            if _job == "法":
                _, _set = tuple(
                    (np.squeeze(skill_set[skill_set["Id"] == _skill_set].values))
                )
                if len(set(_set)) != len(set(_set) - healer_skill):
                    _job = "補"
            set_str += _attr + _job + " / "
        set_char.append(set_str[:-3])
    return pd.DataFrame(set_char)


asset_path = "./TextAsset/"
text_assets = os.listdir(asset_path)

###################################################
# 中文名稱對照
name = pd.read_json(asset_path + "String_ChineseTraditional.json", encoding="utf8")

# 角色資訊
char_cols = ["Id", "JobType", "Attr", "SklActSet"]
char = pd.read_json(asset_path + "Char.json").reindex(columns=char_cols)

char_stat_cols = ["Char", "Show"]
char_stat = pd.read_json(asset_path + "CharStat.json").reindex(columns=char_stat_cols)

char_show_cols = ["Id", "Name"]
char_show = pd.read_json(asset_path + "CharShow.json").reindex(columns=char_show_cols)

# 關卡資訊
hunt_cols = ["Grp", "Num", "Name", "LvMin", "LvMax", "BtlSet"]
hunt = pd.read_json(asset_path + "Hunt.json").reindex(columns=hunt_cols)

# 對戰組合
btl_set = pd.read_json(asset_path + "BtlSet.json")

# 技能與技能組合
skill_cols = ["Id", "Name", "Desc"]
skill = pd.read_json(asset_path + "SklAct.json").reindex(columns=skill_cols)

skill_set_cols = ["Id", "SklAct"]
skill_set = pd.read_json(asset_path + "SklSet.json").reindex(columns=skill_set_cols)

###################################################
# 關卡名稱改中文
hunt["Name"].replace(name.set_index("Id").to_dict()["Str"], inplace=True)
hunt.rename(columns={"Name": "名稱"}, inplace=True)

# 角色名稱改中文
char_show["Name"].replace(name.set_index("Id").to_dict()["Str"], inplace=True)

# 角色職業與屬性改中文
char["JobType"].replace({1: "戰", 2: "弓", 3: "法", 4: "坦"}, inplace=True)
char["Attr"].replace({1: "火", 2: "木", 3: "水", 4: "無"}, inplace=True)

# 技能說明與技能名稱改中文
skill["Name"].replace(name.set_index("Id").to_dict()["Str"], inplace=True)
skill["Desc"].replace(name.set_index("Id").to_dict()["Str"], inplace=True)

###################################################
# 角色名稱與編號整合
char_stat["Show"].replace(char_show.set_index("Id").to_dict()["Name"], inplace=True)

# 角色編號與職業屬性整合
char["Name"] = char["Id"]
char["Name"].replace(char_stat.set_index("Char").to_dict()["Show"], inplace=True)

# 過濾出回復相關的技能
healer_skill = set(skill[skill["Desc"].str.contains("回復")]["Id"])

# 關卡資訊欄位整合
hunt["關卡"] = hunt["Grp"].astype(str) + "-" + hunt["Num"].astype(str)
hunt["等級範圍"] = hunt["LvMin"].astype(str) + "-" + hunt["LvMax"].astype(str)
del hunt["Grp"], hunt["Num"], hunt["LvMin"], hunt["LvMax"]

# 關卡資訊內的對戰組合替換
hunt[["Set1", "Set2"]] = pd.DataFrame(hunt["BtlSet"].tolist())
del hunt["BtlSet"]

# 處理對戰組合資料
hunt[["對戰組合"]] = (
    process_char_of_btl_set("Set1") + "\r\n" + process_char_of_btl_set("Set2")
)
del hunt["Set1"], hunt["Set2"]

###################################################
from styleframe import StyleFrame

StyleFrame(hunt).to_excel("excel_output.xlsx").save()
